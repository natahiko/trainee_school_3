from lxml import html
import pandas
from pandas import DataFrame
import re
from datetime import datetime
import matplotlib.pyplot as plt


def get_trs_from_file(filepath):
    # read file in one string
    f = open(filepath, 'r')
    str = f.read()
    # create parser
    parser = html.fromstring(str)
    return parser.xpath('/html/body/table/tbody/tr')

def listsum(numList):
   if len(numList) == 1:
        return numList[0]
   else:
        return numList[0] + listsum(numList[1:])

def analyse_last_summ(filepath_xls):
    rate = {}
    #read the summs, start of trip and time from table
    trs = get_trs_from_file(filepath_xls)
    for tr in trs[1:]:
        if not tr.find_class("group"):
            if tr[20].text_content() != '00:00':
                arrive = int(tr[18].text_content()[:-3])
                if rate.get(arrive)==None:
                    rate[arrive] = []
                sec = int(tr[20].text_content()[:-3])*60+int(tr[20].text_content()[3:])
                rate.get(arrive).append(float(tr[7].text_content().replace(',', '.'))/sec)
    avg = 0
    #create an array with avg summ per one second in different hours
    for key, value in rate.items():
        rate[key] = listsum(value)/len(value)
        avg += rate[key]
    #calculate the avarage amount of 1 second
    avg = avg/len(rate)
    return rate, avg

def correct_input(user_input):
    if user_input==0:
        return True
    m1 = re.match("(^\d\d:\d\d \d\d:\d\d)", user_input)
    m2 = re.match("(^\d\d:\d\d)", user_input)
    if m1: return True
    if m2: return True
    return False

def make_user_input(rate, avg):
    user_input = 1
    print(avg)
    while user_input!=0:
        print('Введите время посадки и время завершения заказа (HH:MM HH:MM) или длитедьность поездки(MM:SS) \nДля выхода введите 0')
        user_input = input()
        while not correct_input(user_input):
            user_input = input('Введите еще раз: ')
        if len(user_input)<6:
            result = (int(user_input[:-3])+1)*60*avg
        else:
            user_input = user_input.split(' ')
            date1 = datetime.strptime(user_input[0], '%H:%M')
            date2 = datetime.strptime(user_input[1], '%H:%M')
            res = (date2-date1).seconds
            result = rate[int(user_input[0][:-3])]*res
        result = int(round(result))
        print(result)

def get_res(start, end, rate):
        date1 = datetime.strptime(start, '%H:%M')
        date2 = datetime.strptime(end, '%H:%M')
        res = (date2-date1).seconds
        result = rate[int(start[:-3])]*res
        return int(round(result))

def make_analyse(rate):
    roar = []
    names = []
    summ = 0
    amount = 0
    more = 0
    less = 0
    fifth = 0
    trs = get_trs_from_file('report.xls')
    for tr in trs[1:]:
        if not tr.find_class("group"):
            try:
                res = float(tr[7].text_content().replace(',', '.'))
            except:
                res = 0
            if res!=0:
                my_res = get_res(tr[18].text_content(), tr[21].text_content(), rate)
            else:
                my_res = 0
            if my_res>res and res!=0:
                ro = round(res/my_res,2)
                roar.append(ro)
                names.append(tr[18].text_content()[:-3])
                if ro>0.7: more+=1
                elif ro<0.3: less+=1
                if ro>0.5: fifth+=1
                summ+=ro
                amount+=1
            elif res!=0:
                ro = round(my_res/res,2)
                roar.append(ro)
                names.append(tr[18].text_content()[:-3])
                if ro>0.7: more+=1
                elif ro<0.3: less+=1
                if ro>0.5: fifth+=1
                summ += ro
                amount+=1
    print(summ/amount)
    print("total: "+str(amount))
    print("more than 50%: "+str(fifth))
    print("more than 70%: "+str(more))
    print("less than 30%: "+str(less))
    plt.plot(roar, names, marker='.', linewidth=0)
    plt.show()



if __name__ == '__main__':
    rate, avg = analyse_last_summ('report.xls')
    #make_user_input(rate, avg)
    make_analyse(rate)


