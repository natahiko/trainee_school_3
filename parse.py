from lxml import html
import csv
import pandas
import matplotlib.pyplot as plt
from math import *
import os
import logging

filepath_csv = 'results/csv.csv'
filepath_xls = 'report.xls'


def get_trs_from_file(filepath):
    # read file in one string
    f = open(filepath, 'r')
    str = f.read()
    # create parser
    parser = html.fromstring(str)
    return parser.xpath('/html/body/table/tbody/tr')


def create_dict():
    dictionary = {}
    trs = get_trs_from_file(filepath_xls)
    driver_id = 0
    for tr in trs[1:]:
        # if tr about driver, set new driver id
        if tr.find_class("group"):
            driver_id = tr.get('data-guid')
        else:
            # set in dict new report
            try:
                dictionary[tr.get('data-guid')] = {'позывной': tr[0].text_content(),
                                                   'номер': tr[1].text_content(),
                                                   'заказ': tr[2].text_content(),
                                                   'способ оплаты': tr[5].text_content(),
                                                   'сумма': float(tr[7].text_content().replace(',', '.')),
                                                   'комиссия с водителя': float(
                                                       tr[10].text_content().replace(',', '.')),
                                                   'условия работы': tr[13].text_content(),
                                                   'id водителя': driver_id}
            except Exception as e:
                logging.log(e)
    return dictionary


def write_in_csv(dictionary):
    if not os.path.exists('results'):
        os.makedirs('results')
    with open(filepath_csv, 'w', newline='', encoding="utf-8") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(
            ['id', 'позывной', 'номер', 'заказ', 'способ оплаты', 'сумма', 'комиссия с водителя', 'условия работы',
             'id водителя'])
        for id, value in dictionary.items():
            row = [id, value['позывной'], value['номер'], value['заказ'], value['способ оплаты'], value['сумма'],
                   value['комиссия с водителя'], value['условия работы'], value['id водителя']]
            writer.writerow(row)


def check_summs_and_commisions(df):
    df_grouped = df.groupby(['id водителя'])
    df_grouped = df_grouped.agg({'сумма': ['sum'], 'комиссия с водителя': ['sum']})
    trs = get_trs_from_file(filepath_xls)
    for tr in trs[1:]:
        if tr.find_class("group"):
            ts = float(tr[1].text_content().replace(',', '.'))
            tc = float(tr[4].text_content().replace(',', '.'))
            try:
                mys = round(df_grouped['сумма']['sum'][tr.get('data-guid')], 2)
                myc = round(df_grouped['комиссия с водителя']['sum'][tr.get('data-guid')], 2)
            except:
                continue
            if round(fabs(ts - mys), 2) > 0.01:
                print('uncorrect summ id водителя=' + tr.get('data-guid') + ": in table:" + str(
                    ts) + ", calculated: " + str(mys))
            if round(fabs(tc - myc), 2) > 0.01:
                print('uncorrect commission id водителя=' + tr.get('data-guid') + ": in table:" + str(
                    tc) + ", calculated: " + str(myc))


def save_order_amount_chart(filepath, df):
    names = []
    values = []
    df = df.set_index(pandas.to_datetime(df['заказ']))
    df = df.resample('5T')
    for i in df.__iter__():
        names.append(str(i[0])[11:-3] + ' ')
        values.append(len(df.get_group(i[0])))
    plt.figure(figsize=(170.0, 15.0))
    plt.plot(names, values)
    plt.ylabel('orders amount', fontsize=14)
    plt.xlabel('time periods', fontsize=14)
    plt.grid(True)
    plt.savefig(filepath, bbox_inches='tight')


def probability_distribution_functions(filepath, filepath2, df):
    # create array with summ
    values = []
    for i in df['сумма']:
        values.append(i)
    nbins = len(values)
    plt.figure(figsize=(25.0, 15.0))
    plt.xlim(-1, max(values))
    plt.grid(True)

    plt.hist(values, bins=len(values), density=True, histtype='step', cumulative=True, color='purple')
    plt.savefig(filepath, bbox_inches='tight')

    plt.hist(values, color='green', alpha=0.7)
    plt.savefig(filepath2, bbox_inches='tight')


def analyse():
    df = pandas.read_csv(filepath_csv)
    not_free = df[df['сумма'] > 0.0]
    print('Количство заказов с суммой больше нуля: ' + str(len(not_free)))
    cash = df[df['способ оплаты'] == 'Наличные']
    cashless = df[df['способ оплаты'] == 'Безналичные']
    print('Количество наличных заказов: ' + str(len(cash)))
    print('Количество безналичных заказов: ' + str(len(cashless)))

    # compare calculatable summs with summs in table
    check_summs_and_commisions(not_free)

    # order amount chart by time
    save_order_amount_chart("results/order_amount_chart.svg", df)

    # cdf pdf
    probability_distribution_functions("results/probability_functions.svg", "results/distribution_functions.svg",
                                       not_free)


if __name__ == '__main__':
    dictionary = create_dict()
    write_in_csv(dictionary)
    analyse()
